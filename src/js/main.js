//=../../bower_components/jquery/dist/jquery.js
//=../libs/uikit.js
//=../libs/uikit-icons.js
//=../libs/datepicker.js

$(function () {
    $('.blur-modal .btn').on('click', function () {
        $('.root').addClass('blur');
    });

    $('.blur-modal #modal-center').on('click', function () {
        $('.root').removeClass('blur');
    });

    $('.blur-modal .uk-modal-dialog > form').on('click', function (event) {
        event.stopPropagation();
    });

    $('[data-toggle="datepicker"]').datepicker({
        language: 'ru-Ru',
        format: 'dd/mm/yyyy',
        weekStart: 1,
        daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        monthsShort: ['Янв', 'Фев', 'Мрт', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Нбр', 'Дек'],
        autoHide: true,
        autoPick: true
    });

    $('.task-checkbox').on('click', function () {
        var checkbox = $('.uk-checkbox');

        if ($(this).find(checkbox).is(':checked')) {
            $(this).closest('tr').css({
                'background-color': 'rgba(134, 227, 171, 0.1)',
                'color': '#AFAFAF'
            });

            $('.uk-link-reset').attr('disabled', 'true');
        } else {
            $(this).closest('tr').css({
                'background-color': 'transparent',
                'color': 'inherit'
            });

            $('.uk-link-reset').removeAttr('disabled');
        }
    });


    $('.main-category').on('change', function () {

        if ($(this).find('.uk-checkbox').prop('checked')) {
            console.log('hello');

            $(this).siblings('.sub-categories').find('.uk-checkbox').prop({checked: true});
        } else {
            console.log('hey');
            $(this).siblings('.sub-categories').find('.uk-checkbox').prop({checked: false});
        }

    });

    $('.sub-category').each(function () {
        $(this).on('change', function () {
            console.log('heeey');
            var mainCategory = $(this).closest('.sub-categories').siblings('.main-category').children('.uk-checkbox');

            if (mainCategory.prop('checked')) {
                mainCategory.prop({checked: false});
            }

            // function checkSiblings(el) {
            //
            //     var parent = el.parent().parent(),
            //         all = true;
            //
            //     el.siblings().each(function () {
            //         return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
            //     });
            //
            //     if (all && checked) {
            //
            //         parent.children('input[type="checkbox"]').prop({
            //             indeterminate: false,
            //             checked: checked
            //         });
            //
            //         checkSiblings(parent);
            //
            //     } else if (all && !checked) {
            //
            //         parent.children('input[type="checkbox"]').prop("checked", checked);
            //         parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked').length > 0));
            //         checkSiblings(parent);
            //
            //     } else {
            //
            //         el.parents("li").children('input[type="checkbox"]').prop({
            //             indeterminate: true,
            //             checked: false
            //         });
            //
            //     }
            //
            // }
        });
    })
});







